/**
 * Created by lindakeating on 18/08/2014.
 */
var recorder = null;
var my_media = null;
var audioPath = null;
var audioContext = null;
var playBack = null;

function playAudio(src){
    setSongTitle(src);
    createFileDirectory();
}

function setSongTitle(src){
    window.srcSong = src;
    if(src == "/audio/RobotR33.wav"){
        window.songName = "robotWords";
        window.song = "robot";
    }
    if(src == "/audio/CorNaNAinmhithe.mp3"){
        window.songName = "bualadhBosWords";
        window.song = "bualadhBos";
    }
    if(src == "/audio/CoisirCheoil.mp3"){
        window.songName = "coisirCheoilWords";
        window.song = "coisirCheoil";
    }
    var dateTime = new Date();
    var d = dateTime.toLocaleString().replace(/\W/g, '_');
    window.audioRecSrc =  window.song +d+'.wav';
}
///*************************************************************************************************************
/// Create a directory system
function createFileDirectory(){
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
}

/// Create a new file
function gotFS(fileSystem){
    fileSystem.root.getFile(window.audioRecSrc,{create: true, exclusive: false}, getFileWin, null );
}

//
function getFileWin(fileEntry){
    audioPath = fileEntry.fullPath;
   // recorder = new Media(audioPath, onMediaCallSuccess, onMediaCallError);
    recorder = new Media('documents://'+window.audioRecSrc, onMediaCallSuccess, onMediaCallError);
    play();
}

function onMediaCallSuccess(){
    console.log( 'New Recording successfully created successfully');
    createAudioContext();
}

function onMediaCallError(){
    console.log( 'Error creating new media');
}

///****************************************************************************************************************
///
function play(){
    if(my_media === null){
        my_media = new Media(window.srcSong, onSuccess, fail, mediaStatus);
        my_media.play();
        recorder.startRecord();
    }
    else{
        my_media.play();
    }
}

function onSuccess(){
    console.log("playAudio(): Audio Success");
}

function fail(error){
    console.log("error : " + error.code);
}

function mediaStatus(){
    if(arguments[0] == 4){
        stopAudio();
    }
}
///***************************************************************************************************************

function stopAudio(){
    // need to implement the stop Audio Function

    if(my_media){
        recorder.stopRecord();
        recorder.release();
        my_media.stop();
        my_media.release();
        my_media = null;
    }
    $.mobile.changePage("#recordings", {transition: "slide"});
    // load backing track & vocal recording into buffers

}

function createAudioContext(){
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
    window.URL = window.URL || window.webkitURL;
    audioContext = new AudioContext;
    getDirectory();
}

function getDirectory(){
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, getFileSystem, fail);
}

function getFileSystem(directory){
    directory.root.getFile(audioPath, null, getVocalFile, fail);
}

function getVocalFile(fileEntry){
    fileEntry.file(readVocalsToBuffer, fail);
}

function readVocalsToBuffer(file){
    var reader = new FileReader();
    reader.onloadend = function(evt){
        var x = audioContext.decodeAudioData(evt.target._result, function(buffer){
            if(!buffer){
                console.log('error decoding file to Audio Buffer');
                return;
            }
            window.voiceBuffer = buffer;
            console.log(window.voiceBuffer);
            loadBuffers();
        });
    }
    reader.readAsArrayBuffer(file);
}

function loadBuffers(){
    try{
        var bufferLoader = new BufferLoader(
            audioContext,
            [
                "."+window.srcSong
            ],
            createOffLineContext
        );
        bufferLoader.load()
    }
    catch(e){
        console.log(e.message);
    }
}

function createOffLineContext(bufferList){
    var offline = new webkitOfflineAudioContext(2, window.voiceBuffer.length, 44100);
    var vocalSource = offline.createBufferSource();
    vocalSource.buffer = window.voiceBuffer;
    vocalSource.connect(offline.destination);

    var backing = offline.createBufferSource();
    backing.buffer = bufferList[0];
    backing.connect(offline.destination);

    vocalSource.start(0);
    backing.start(0);

    offline.oncomplete = function(ev){
        window.renderedFile = ev.renderedBuffer;
        var bufferR = ev.renderedBuffer.getChannelData(0);
        var bufferL = ev.renderedBuffer.getChannelData(1);
        var interleaved = interleave(bufferL, bufferR);
        var dataview = encodeWAV(interleaved);
        window.audioBlob = new Blob([dataview], {type: 'Wav'});
        saveFile();
    }

    offline.startRendering();

}

function interleave(inputL, inputR){
    var length = inputL.length + inputR.length;
    var result = new Float32Array(length);

    var index = 0,
        inputIndex = 0;

    while (index < length){
        result[index++] = inputL[inputIndex];
        result[index++] = inputR[inputIndex];
        inputIndex++;
    }
    return result;
}

function saveFile(){
    window.voiceBuffer = null;
    window.renderedFile = null;
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onFSSuccess, fail);
}

function onFSSuccess(fs){
    fileSystem = fs;
    createFile(fs);
}

function createFile(fs){
    fs.root.getFile(window.audioRecSrc,{create: true}, successFile, fail);
}

function successFile(file){
    file.createWriter(function(fileWriter){
        fileWriter.write(audioBlob);
    }, fail);
    checkDirectory();
}

function checkDirectory(){
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, dirSuccess, fail);
}

function dirSuccess(fileSystem){
    doDirectoryListing(fileSystem);
}

function doDirectoryListing(fileSystem){
    var dirReader = fileSystem.root.createReader();
    dirReader.readEntries(gotFiles, fail);
}

function gotFiles(entries){
    $('#recordingBody').empty();
    var s = "";
    for(var i=0; i<entries.length; i++){
        console.log(entries[i]);
        if(entries[i].isFile){
            $('#recordingBody').append('<li>' +
                '<a href="#" data-role="button" onclick="playBackRecording(\''+entries[i].fullPath+'\')">' +
                '<i class="fa fa-play fa-2x"></i>' +
                '</a>' +
                '<a href="#" data-rol="button" onclick="deleteRecording(\''+entries[i].fullPath+'\')">' +
                '<i class="fa fa-trash-o fa-2x"></i>'+
                '</a>'+
                '</li>');
        }
    }
    window.audioBlob = null;
}

///******************************************************************************************************************///
function playBackRecording(rec){

    playBack = new Media('documents://'+rec, playBackSuccess, onMediaCallError);
    playBack.play();
}

function playBackSuccess(){
    playBack.release();
    playBack = null;
    console.log('playBack Successful');
}

function deleteRecording(rec){
    window.deleteFile = rec;
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, delFileSystemSuccess, fail );
}

function delFileSystemSuccess(fs){
    fs.root.getFile(window.deleteFile, {create: false}, gotFileSuccess, fail);
}

function gotFileSuccess(fileEntry){
    fileEntry.remove(successFileRemoved, fail)
}

function successFileRemoved(){
    checkDirectory();
}

//**************************************************************************************************
