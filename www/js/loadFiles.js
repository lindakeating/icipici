/**
 * Created by lindakeating on 16/08/2014.
 */
// create a new File once each recording and wav encoding is completed
function createFileDirectory(){
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onFSSuccess, onError);
}

function onFSSuccess(fs){
    fileSystem = fs;
    createFile(fs);
}

function createFile(fs){
    var dateTime = new Date().toISOString();
    fs.root.getFile(song+dateTime+".wav",{create: true}, successFile);
}

function successFile(file){
    file = audioBlob;
}

// check what files exist in the directory
function checkDirectory(){
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, dirSuccess, onError);
}

function dirSuccess(fileSystem){
    doDirectoryListing(fileSystem);
}

function doDirectoryListing(fileSystem){
    var dirReader = fileSystem.root.createReader();
    dirReader.readEntries(gotFiles, onError);
}

function gotFiles(entries){
    var s = "";
    for(var i=0; i<entries.length; i++){
        console.log("fullPath: " + entries[i].fullPath);
        if(entries[i].isFile){
            console.log(entries[i].getMetadata());
        }
    }
}